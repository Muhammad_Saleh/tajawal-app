import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SharedModule} from '../../shared/shared.module';
import {SearchComponent} from './search.component';
import {APP_BASE_HREF} from '@angular/common';
import {appRoutes} from '../../app.routes';
import {ListingModule} from '../../listing/listing.module';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
      ],
      imports: [
        SharedModule,
        ListingModule,
        appRoutes
      ],
      declarations: [SearchComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
