import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'tj-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchFormGroup: FormGroup;
  today = new Date();

  constructor(private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.searchFormGroup = this.fb.group({
      fromDate: [''],
      toDate: [''],
    });
  }

  public search() {
    const dateFormat = 'DD-MM-YYYY';
    const fromDate = moment(this.searchFormGroup.controls.fromDate.value).format(dateFormat);
    const toDate = moment(this.searchFormGroup.controls.toDate.value).format(dateFormat);
    this.router.navigate(['/search', fromDate, toDate]);
  }

}
