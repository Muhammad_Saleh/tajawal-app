import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MaterialModule, MdDatepickerModule, MdNativeDateModule, MdSidenavModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FilterByPipe} from '../pipes/FilterBy';
import {OrderByPipe} from '../pipes/OrderBy';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MdSidenavModule
  ],
  declarations: [
    FilterByPipe,
    OrderByPipe
  ],
  exports: [
    CommonModule,
    BrowserAnimationsModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MdSidenavModule,
    FilterByPipe,
    OrderByPipe
  ]
})
export class SharedModule {
}
