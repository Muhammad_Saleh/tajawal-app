import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CommonModule} from '@angular/common';
import {ListingComponent} from './listing.component';
import {HotelsListComponent} from '../listing/components/hotels-list/hotels-list.component';
import {HotelComponent} from '../listing/components/hotel/hotel.component';
import {SharedModule} from '../shared/shared.module';
import {HotelsService} from '../services/hotels/hotels.service';
import {HttpModule} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import * as moment from 'moment';

@Injectable()
export class ActivatedRouteStub {
  private subject = new BehaviorSubject(this.testParams);
  params = this.subject.asObservable();

  private _testParams: {};
  get testParams() {
    return this._testParams;
  }

  set testParams(params: {}) {
    this._testParams = params;
    this.subject.next(params);
  }
}

describe('ListingComponent', () => {
  let component: ListingComponent;
  let fixture: ComponentFixture<ListingComponent>;
  let mockActivatedRoute;

  let hotel1 = {
    'name': 'Media One Hotel',
    'price': 102.2,
    'city': 'dubai',
    'availability': [{'from': '10-10-2020', 'to': '15-10-2020'}, {
      'from': '25-10-2020',
      'to': '15-11-2020'
    }, {'from': '10-12-2020', 'to': '15-12-2020'}]
  };

  beforeEach(async(() => {
    mockActivatedRoute = new ActivatedRouteStub();

    TestBed.configureTestingModule({
      declarations: [HotelsListComponent, ListingComponent, HotelComponent],
      imports: [CommonModule, HttpModule, SharedModule],
      providers: [HotelsService, {provide: ActivatedRoute, useValue: mockActivatedRoute}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingComponent);
    mockActivatedRoute.testParams = {fromDate: '11-10-2020', toDate: '14-10-2020'};
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should check if a hotel is bookable or not', () => {
    expect(component.canBookHotel(hotel1, moment('11-10-2020', 'DD-MM-YYYY'), moment('14-10-2020', 'DD-MM-YYYY'))).toBeTruthy();
  });

  it('Hotel is not bookable in this duration', () => {
    expect(component.canBookHotel(hotel1, moment('09-10-2020', 'DD-MM-YYYY'), moment('14-10-2020', 'DD-MM-YYYY'))).toBeFalsy();
  });

  it('Invalid date in hotel booking checking', () => {
    expect(component.canBookHotel(hotel1, 'hello', moment('14-10-2020', 'DD-MM-YYYY'))).toBeFalsy();
  });

  it('Should toggle name sorting', () => {
    expect(component.orderBy).toEqual('+name');
    component.toggleNameSort();
    expect(component.orderBy).toEqual('-name');
    component.toggleNameSort();
    expect(component.orderBy).toEqual('+name');
  });

  it('Should toggle price sorting', () => {
    expect(component.orderBy).toEqual('+name');
    component.togglePriceSort();
    expect(component.orderBy).toEqual('+price');
    component.togglePriceSort();
    expect(component.orderBy).toEqual('-price');
  });
});
