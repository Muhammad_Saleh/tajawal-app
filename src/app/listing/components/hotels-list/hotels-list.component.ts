import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tj-hotels-list',
  templateUrl: './hotels-list.component.html',
  styleUrls: ['./hotels-list.component.scss']
})
export class HotelsListComponent implements OnInit {
  @Input() hotels = [];
  @Input() nameFilter = '';
  @Input() priceFilter = 0;
  @Input() orderBy = '+name';

  constructor() {
  }

  ngOnInit() {
  }

}
