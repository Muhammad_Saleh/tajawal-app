import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SharedModule} from '../../../shared/shared.module';
import {HotelsListComponent} from './hotels-list.component';
import {ListingComponent} from '../../../listing/listing.component';
import {HotelComponent} from '../../../listing/components/hotel/hotel.component';

describe('HotelsListComponent', () => {
  let component: HotelsListComponent;
  let fixture: ComponentFixture<HotelsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HotelsListComponent, ListingComponent, HotelComponent],
      imports: [SharedModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
