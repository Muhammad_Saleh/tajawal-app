import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tj-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {
  @Input() hotel = {
    name: '',
    city: '',
    price: ''
  };

  constructor() {
  }

  ngOnInit() {
  }

}
