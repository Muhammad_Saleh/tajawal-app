import {Component, OnInit} from '@angular/core';
import {HotelsService} from '../services/hotels/hotels.service';
import 'rxjs/add/operator/take';
import * as _ from 'lodash';
import {ActivatedRoute} from '@angular/router';
import * as moment from 'moment';
import {Moment} from 'moment';

@Component({
  selector: 'tj-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  hotels = [];
  minPrice = 0;
  maxPrice = 0;
  totalNights = 0;
  orderBy = '+name';
  fromDate: Moment;
  toDate: Moment;

  constructor(private hotelsService: HotelsService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.take(1).subscribe((params) => {
      this.fromDate = moment(params.fromDate, 'DD-MM-YYYY');
      this.toDate = moment(params.toDate, 'DD-MM-YYYY');
      this.totalNights = this.toDate.diff(this.fromDate, 'days');
    });

    this.hotelsService.getHotels()
      .take(1)
      .subscribe((data) => {
        this.hotels = data.filter((hotel) => this.canBookHotel(hotel, this.fromDate, this.toDate));
        if (_.minBy(this.hotels, 'price') && _.minBy(this.hotels, 'price').price &&
          _.maxBy(this.hotels, 'price') && _.maxBy(this.hotels, 'price').price) {
          this.minPrice = Number((_.minBy(this.hotels, 'price').price * this.totalNights).toFixed(0));
          this.maxPrice = Math.ceil((_.maxBy(this.hotels, 'price').price * this.totalNights));
        }

        this.hotels.map((hotel) => {
          hotel.price = Number(hotel.price * this.totalNights).toFixed(2);
        });
      });
  }

  public toggleNameSort() {
    (this.orderBy === '+name') ? this.orderBy = '-name' : this.orderBy = '+name';
  }

  public togglePriceSort() {
    (this.orderBy === '+price') ? this.orderBy = '-price' : this.orderBy = '+price';
  }

  canBookHotel(hotel, fromDate, toDate) {
    let canBook = false;

    if (hotel && moment.isMoment(fromDate) && moment.isMoment(toDate)) {
      hotel.availability.map((dates) => {
        const hotelAvailableFrom = moment(dates.from, 'DD-MM-YYYY');
        const hotelAvailableTo = moment(dates.to, 'DD-MM-YYYY');

        if (moment(fromDate).isBetween(hotelAvailableFrom, hotelAvailableTo, null, '[]') &&
          moment(toDate).isBetween(hotelAvailableFrom, hotelAvailableTo, null, '[]')) {
          canBook = true;
        }
      });
    }

    return canBook;
  }
}
