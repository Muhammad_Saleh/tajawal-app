import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListingComponent} from '../listing/listing.component';
import {HotelsService} from '../services/hotels/hotels.service';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../shared/shared.module';
import {HotelsListComponent} from './components/hotels-list/hotels-list.component';
import {HotelComponent} from './components/hotel/hotel.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    SharedModule
  ],
  declarations: [ListingComponent, HotelsListComponent, HotelComponent],
  providers: [HotelsService]
})
export class ListingModule {
}
