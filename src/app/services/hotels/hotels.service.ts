import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HotelsService {
  hotels = [];

  constructor(private http: Http) {
  }

  getHotels() {
    return this.http.get(`https://api.myjson.com/bins/tl0bp`)
      .map(response => response.json().hotels);
  }

}
