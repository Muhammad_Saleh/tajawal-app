import {RouterModule} from '@angular/router';
import {SearchComponent} from './components/search/search.component';
import {ListingComponent} from './listing/listing.component';


const routes = [
  {path: '', component: SearchComponent},
  {path: 'search/:fromDate/:toDate', component: ListingComponent},
  {path: '**', redirectTo: ''},
];


export const appRoutes = RouterModule.forRoot(
  routes
);
