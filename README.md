# Tajawal App

Hotels listing and search for Tajawal Interview

## Usage

1. Make sure you have Node.js installed
2. run `npm install`
3. run `ng serve`
4. open `http://localhost:4200`

## Build

Run `ng build` to build the project.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Components Structure
```
App Module [Main Module]
    - App Component (Header and router outlet)
    - Header Component
    - Search Component [Used in the home search page]

    - Listing Module
        - Listing Component [Smart Component (containing all of the listing logic)]
        - Hotels List Component [Dumb Component - Responsible for the listing grid and passing only the filtering properties and the ordering properties]
        - Hotel Component [Dumb Component - It has the hotel card which is repeated in the list]
        
    - Shared Module [Holds common imports and exports like material components to be used everywhere]

    - Hotels Service [Responsible of calling the API]

    - Filter Pipe (Imported from FuelUI Project)
    - OrderBy Pipe (Imported from an online project)
```

## Dependencies
- Moment.js for handling dates parsing and comparison
- Lodash for working with datasets
- Angular Material for UI components
- Filter Pipe (Imported from FuelUI Project)
- OrderBy Pipe (Imported from an online project)
